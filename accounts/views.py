from django.shortcuts import render, redirect
from accounts.forms import AccountForm, AccountSignupForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


# Create your views here.


def account_sign_in(request):
    form = AccountForm
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def account_sign_out(request):
    logout(request)
    return redirect("login")


def account_sign_up(request):
    form = AccountSignupForm
    if request.method == "POST":
        form = AccountSignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, user)
                return redirect("home")
            else:
                form.add_error("the passwords do not match")
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
