from django.urls import path
from accounts.views import account_sign_in, account_sign_out, account_sign_up

urlpatterns = [
    path("login/", account_sign_in, name="login"),
    path("logout/", account_sign_out, name="logout"),
    path("signup/", account_sign_up, name="signup"),
]
