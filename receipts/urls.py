from django.urls import path
from receipts.views import (
    all_receipts_view,
    create_receipt,
    all_expense_categories,
    all_accounts,
    create_category_view,
    create_account_view,
)

urlpatterns = [
    path("", all_receipts_view, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", all_expense_categories, name="category_list"),
    path("accounts/", all_accounts, name="account_list"),
    path("categories/create/", create_category_view, name="create_category"),
    path("accounts/create/", create_account_view, name="create_account"),
]
