from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory


# Register your models here.
@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "owner",
        "name",
        "number",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "purchaser",
        "vendor",
        "total",
        "tax",
        "date",
        "account",
        "category",
    )


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "owner",
        "name",
    )
