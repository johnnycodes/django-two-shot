from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import (
    CreateReceiptForm,
    CreateExpenseCategoryForm,
    CreateAccountForm,
)
from django.contrib.auth.decorators import login_required


@login_required
# Create your views here.
def all_receipts_view(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/home.html", context)


def create_receipt(request):
    form = CreateReceiptForm
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            ind = form.save(commit=False)
            ind.purchaser = request.user
            ind = ind.save()
            return redirect("home")
    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required
def all_expense_categories(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user.id)
    context = {
        "expenses": expenses,
    }
    return render(request, "receipts/expense_categories.html", context)


def all_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category_view(request):
    form = CreateExpenseCategoryForm
    if request.method == "POST":
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            exp = form.save(commit=False)
            exp.owner = request.user
            exp.save()
            return redirect("category_list")
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


def create_account_view(request):
    form = CreateAccountForm
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            exp = form.save(commit=False)
            exp.owner = request.user
            exp.save()
            return redirect("account_list")
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
